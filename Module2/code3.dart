/* Author: Tshegofatso Morare
   Email : Tshego.tt.morare@gmail.com
   Phone : +27 61 188 4553
 */

void main() {
  App application = new App();
  application.details();
}

class App {
  var app_name = "Takealot";
  var categoryOrSector = "E-commerce";
  var developer = "takealot";
  int yearWon = 2021;

  void details() {
    //change the case of variables and print
    print("name of app:");
    print(app_name.toUpperCase());
    print("category or sector of app:");
    print(categoryOrSector);
    print("developer:");
    print(developer);
    print("year it won MtnAppAcademy:");
    print(yearWon);
  }
}
