/* Author: Tshegofatso Morare
   Email : Tshego.tt.morare@gmail.com
   Phone : +27 61 188 4553
 */

void main() {
  //array for past winners
  var winners = [];

  //add the winners since 2012
  //2012 winners
  winners.add("FNB");
  winners.add("Plascon");
  winners.add("ZA");
  winners.add("Gears");
  winners.add("DiscoveryHealthID");
  winners.add("rapidTargets");
  //2013 winners
  winners.add("DSTV");
  winners.add("bookly");
  winners.add("Nedbank app");
  winners.add("pricecheck");
  //2014 winners
  winners.add("supersportsapp");
  //2015 winners
  winners.add("DstvNow");
  winners.add("M4Jam");
  //2016 winners
  winners.add("ikhokha.com");
  winners.add("hearZA");
  winners.add("Tuta-me");
  //2017 winners
  winners.add("OrderIn");
  winners.add("SouthAfrican Super Animals");
  winners.add("Shyft");
  winners.add("Hey Jude");
  //2018 winners
  winners.add("be smarta");
  winners.add("cowa bunga");
  winners.add("Ctrl");
  //2019 winners
  winners.add("Digger");
  winners.add("vula");
  winners.add("over");
  winners.add("loc transie");
  winners.add("hydra");

  //sort and print the winners
  winners.sort();
  print(winners);

  //print winning app of 2017 and 2018
  print("2017 winners:");
  print(winners[10]);
  print("2018 winners:");
  print(winners[16]);

  print("number of app from array:");
  print(winners.length);
}
